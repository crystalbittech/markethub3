import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

/*
  Generated class for the UserProspectsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class HttpReqst {
  headers: Headers;
  requestOption:RequestOptions;
  response:{};
//   url:string = "http://localhost:8087/api";
  url:string = "https://markethubapi.herokuapp.com/api";
  
  

  constructor(public http: Http) {
    this.headers  = new Headers();
    this.headers.append("Accept", 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.requestOption = new RequestOptions({ headers: this.headers });
  }


    getRequest(api:string) {
        return new Promise( (resolve, reject) => {
            this.http.get(this.url+api, this.requestOption)
            .subscribe(data => {
            this.response = data.json(); 
            // console.log("Response from service is "+JSON.stringify(this.response));
            resolve(this.response);
            }, error => {
            console.log(error);
            this.response = {message:"Fatal Error: "+error} ;
            reject(this.response);
            });
        });
            
    }

    postRequest(api:string,request:any) {
        return new Promise( (resolve, reject) => {
        // console.log("Sending with dtaa "+JSON.stringify(request))
        this.http.post(this.url+api, request, this.requestOption)
            .subscribe(data => {
            this.response = data.json();
            resolve(this.response);
            }, error => {
            console.log(error);
            this.response = {message:"Fatal Error: "+error} ;
            reject(this.response);
            });
        });

    }

    putRequest(api:string,request:any){
        return new Promise( (resolve, reject) => {
            // console.log("Sending with dtaa "+JSON.stringify(request))
            this.http.put(this.url+api, request, this.requestOption)
                .subscribe(data => {
                this.response = data.json();
                // console.log("Response from service is "+JSON.stringify(this.response));
                resolve(this.response);
                }, error => {
                console.log(error);
                this.response = {message:"Fatal Error: "+error} ;
                reject(this.response);
                });
            });
    }

    deleterequest(){
        
    }



}
