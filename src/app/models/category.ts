export class Category {
    id?:string;
    uid?: string;
    categoryName?:string;
    description?:string;
    classtype?:string;
    image?: string;
    dateAdded?:number;
}