export interface User {
    uid?: string;
    firstname?:string;
    lastname?:string;
    email?: string;
    password?: string;
    phoneNumber?:string;
    birthdate?: number;
    address?:string;
    serviceCategory?:string;
    username?: string;
    charge?:number;
    description?:string;
    photo?: string;
    reglocation?:{name:string,lat:number,lng:number,center:string};
}

export interface UserSession extends User{
  usrType?:string;
  sessionID?: string;
  isUserLoggedIn?:boolean;
  merchantID?:string;
  cartDetails?:{uid?:string,totalPrice?:number,cartItems?:any}
}

// export interface MerchantUser extends User{
//     usrType?:string;
//     businessName?:string;
//     bizPhoneNumber?:string;
//     siteSubdomain?:string;
//     bizAdress?:string;
//     bizCategory?:string;
//     bizState?:string;
//     bizLGA?:string;
//     bizStartDate?:number;
// }

