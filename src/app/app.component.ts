import { Component } from '@angular/core';
import {User, UserSession} from './models/user';
import {DatasharingService} from './services/datasharing/datasharing.service';
import {Router,ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isUserLoggedIn:boolean=false;
  usrEmail:String='';
  userSession:UserSession={};
  hideforStoreMenu:boolean=false;
  cartInfo:UserSession["cartDetails"];

  constructor(
    private dataSharer:DatasharingService,
    private router:Router
  ){
    this.dataSharer.userSession.subscribe( value => {
      this.userSession = value;
      if(this.userSession!=null){
          this.isUserLoggedIn = this.userSession.isUserLoggedIn;
          this.usrEmail = this.userSession.uid;
          this.cartInfo = this.userSession.cartDetails;
      }else{
          this.isUserLoggedIn = false;
          this.usrEmail = '';
          this.cartInfo = {uid:'',totalPrice:0,cartItems:[]}
      }
    });  
    this.dataSharer.storeEditMode.subscribe(value=>{
        this.hideforStoreMenu = value;
    });

  }

  logOut(){
      this.dataSharer.destroySession()
      this.router.navigate(['login']);
  }


}
