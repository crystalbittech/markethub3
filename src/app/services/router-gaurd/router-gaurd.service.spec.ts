import { TestBed, inject } from '@angular/core/testing';

import { RouterGaurdService } from './router-gaurd.service';

describe('RouterGaurdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterGaurdService]
    });
  });

  it('should be created', inject([RouterGaurdService], (service: RouterGaurdService) => {
    expect(service).toBeTruthy();
  }));
});
