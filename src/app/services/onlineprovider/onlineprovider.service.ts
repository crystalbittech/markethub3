import { Injectable } from '@angular/core';
import {User} from '../../models/user';
import {Product} from '../../models/product';
import {Category} from '../../models/category';
import {Merchant} from '../../models/merchant';
import {HttpReqst} from '../../interface/httprequest';

@Injectable()
export class OnlineproviderService {
  constructor(
    private httpreq:HttpReqst
  ) { }

  signIn(userCreds:User) {
      return this.httpreq.postRequest("/user/signin",userCreds).then(resp=>{
        
        return resp;
      }).catch(err=>{
        console.log(err);
      })
  }
  signUp(userCreds:User) {
    return this.httpreq.postRequest("/user/signup",userCreds).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  addProduct(productDetails:Product) {
    return this.httpreq.postRequest("/product/add",productDetails).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  addCategory(categoryDetails:Category) {
    return this.httpreq.postRequest("/category/add",categoryDetails).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  addStore(merchantDetails:Merchant) {
    
    return this.httpreq.postRequest("/merchant/add",merchantDetails).then(resp=>{
      console.log("sent : "+JSON.stringify(merchantDetails));
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchProducts(filterKey:string) {
    return this.httpreq.getRequest("/product/all/"+filterKey).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchSngProduct(query:string) {
    return this.httpreq.getRequest("/product/"+query).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  searchItem(query:string) {
    return this.httpreq.getRequest("/search/"+query).then(resp=>{
      // console.log(JSON.stringify(resp))
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  addToCart(cartDetails:any) {
    return this.httpreq.postRequest("/session/add",cartDetails).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchSession(usrID:string) {
    return this.httpreq.getRequest("/session/"+usrID).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchStoreDEtails(storeId:string) {
    return this.httpreq.getRequest("/merchant/storeinfo/"+storeId).then(resp=>{
      console.log(JSON.stringify(resp))
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }
  
  sendAuthMail(query:any) {
    return this.httpreq.postRequest("/sendmail",query).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }


  fetchCategory(query:string) {
    return this.httpreq.getRequest("/category/filter/"+query).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fecthStoreUsers(query:string) {
    return this.httpreq.getRequest("/user/storeusers/"+query).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchAllCategory() {
    return this.httpreq.getRequest("/category").then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchMerchants() {
    return this.httpreq.getRequest("/merchant/all").then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  updateMerchantProp(merchantProp:any) {
    return this.httpreq.putRequest("/merchant/updateprop",merchantProp).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchStoreProp(query:string) {
    return this.httpreq.getRequest("/merchant/storeinfo/"+query).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

  fetchStoreID(usrID:string) {
    return this.httpreq.getRequest("/merchant/"+usrID).then(resp=>{
      return resp;
    }).catch(err=>{
      console.log(err);
    })
  }

}
