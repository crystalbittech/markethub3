import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Merchant} from '../../models/merchant';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
import { Product } from '../../models/product';
import { Category } from '../../models/category';
import {DatasharingService} from '../../services/datasharing/datasharing.service';
import {UserSession} from '../../models/user'


@Component({
  selector: 'app-searchresult',
  templateUrl: './searchresult.component.html',
  styleUrls: ['./searchresult.component.css']
})
export class SearchresultComponent implements OnInit {
  searchList:any=[];
  allCategories:[Category];
  categoryList:any=[];
  lat: number = 6.5503636;  lng: number = 3.2998000999999997;  lat2: number = 6.3503636;  lng2: number = 3.6998000999999997;
  mapType:string="roadmap";
  zoomLevel:number=17;
  counteer:number=0;
  userInfo:UserSession={};
  

 
  constructor(
    private routes:Router,
    private roueAct:ActivatedRoute,
    private dataSharer:DatasharingService,
    private backApi:OnlineproviderService
  ) { 
      this.backApi.fetchAllCategory().then(resp=>{
          var respObj:any = resp;
          this.allCategories = respObj;
          for(var i=0;i<this.allCategories.length;i++){
            let isAdded=false; 
            if(i==0){
              this.categoryList.push(this.allCategories[i].categoryName);
              continue;
            }
            for(var j=0;j<this.categoryList.length;j++){
              if(this.allCategories[i].categoryName.toUpperCase()==this.categoryList[j].toUpperCase()){
                isAdded = true;
              }
            }
            if(!isAdded){
              this.categoryList.push(this.allCategories[i].categoryName);
            }
          }
      });
    }
    
  ngOnInit(){
        var queryString = this.roueAct.snapshot.params['data'];
        this.backApi.searchItem(queryString).then(resp=>{
            var respObj:any = resp;
            console.log("Recieved: "+JSON.stringify(respObj.message))
            this.searchList = respObj.message;
        }).catch(err=>{
          
        })
  }
 
  navToProduct(productID){
    this.routes.navigate(['product_detail',{'data':productID}]);
  }
  navToStore(storeID){
    this.routes.navigate(['store',{'data':storeID}]);
  }

  addToCart(prodID){
    var totalAmt = 0;
    var doPush:boolean=true;
    var cartItm = {
      id:prodID.productID,
      prodName:prodID.productName,
      img:'',
      price:prodID.price,
      qty:1
    };
    this.userInfo = this.dataSharer.getSession();
    var cartList:any = this.userInfo.cartDetails.cartItems    
    for(var i=0;i<cartList.length;i++){
      if(cartList[i].id==cartItm.id){
        cartList[i].qty = cartList[i].qty+1;
        doPush = false;
      }
      totalAmt = totalAmt + cartList[i].qty * cartList[i].price
    }
    
    if(doPush){
      cartList.push(cartItm);
      totalAmt = totalAmt + cartItm.qty * cartItm.price
    }

    this.userInfo.cartDetails = {
      uid:this.userInfo.uid,
      totalPrice:totalAmt,
      cartItems:cartList
    }
    console.log("About Pushing: "+JSON.stringify(this.userInfo.cartDetails))
    this.pushCartToDb(this.userInfo.cartDetails);
    this.dataSharer.userSession.next(this.userInfo);
  }

  pushCartToDb(cartData){
    this.backApi.addToCart(cartData).then(resp=>{
      var respObj:any = resp;
      console.log("Recieved: "+JSON.stringify(respObj.message))
    }).catch(err=>{
    
  })
  }
}
