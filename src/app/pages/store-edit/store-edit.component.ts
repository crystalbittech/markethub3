import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {Product} from '../../models/product';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
import {Category} from '../../models/category';
import {UserSession,User} from './../../models/user';
import {DatasharingService} from '../../services/datasharing/datasharing.service';
import { Jsonp } from '@angular/http';

@Component({
  selector: 'app-store-edit',
  templateUrl: './store-edit.component.html',
  styleUrls: ['./store-edit.component.css']
})
export class StoreEditComponent implements OnInit {


newProduct:Product={};
newCategory:Category={};
staffDetail: User={};
myProducts:Array<Product>=[this.newProduct];
myCategories:Array<Category>=[this.newCategory];
myStaffs:Array<User>=[this.staffDetail];
usrSession:UserSession={"isUserLoggedIn":false,"uid":"","merchantID":"","cartDetails":{"uid":"","totalPrice":0,"cartItems":[]}};
navStack = ['BASE'];
navStackOpt={mode:'list',values:''};
activeView:string=this.navStack[0];


colorStack:string[]=[];
sizeStack:string[]=[];
colorPicked:string;
sizePicked:string;
isTopProduct:boolean=false;
pageHeader = {mainTitle:"",subTitle:"",img:''}
pageFooter = [
          {isActivated:true,columnTitle:"Contact Us",columnType:'',columnDescriptn:"Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018"},
          {isActivated:true,columnTitle:"Categories",columnType:'',columnDescriptn:""},
          {isActivated:true,columnTitle:"Links",columnType:'',columnDescriptn:""},
          {isActivated:false,columnTitle:"Help",columnType:'',columnDescriptn:""},
          {isActivated:true,columnTitle:"Newsletter",columnType:'',columnDescriptn:""}
]
propSetings:any = {pageHeader:this.pageHeader,pageFooter:this.pageFooter};


  constructor(
    private router:Router,
    private onLineProv:OnlineproviderService,
    private dataSharer:DatasharingService
  ) { 
    this.dataSharer.storeEditMode.next(true);
    dataSharer.setSession().then(resp=>{
      this.usrSession = this.dataSharer.getSession();
      console.log("Session value  is: "+JSON.stringify(this.usrSession))
      this.fetchProp(this.usrSession.merchantID);
      this.newProduct = {
        productName:"",
        price:null,
        uid:this.usrSession.merchantID,
        sizes:this.sizeStack,
        colors:this.colorStack,
        isTopBrand:false,
        image:'',
        prodCategory:''
      }
      this.newCategory = {
        uid:this.usrSession.merchantID,
        categoryName:''
      }   
    }); 
    
    
  }

  ngOnInit() {
    console.log('Entered Store edit page');
  }

 
  addProduct(){
    this.newProduct.uid = this.usrSession.merchantID;
    this.onLineProv.addProduct(this.newProduct).then(resp=>{
      var respObj:any=resp;
      alert(respObj.message);
      this.updateProductStack(respObj.data);
    });
  }
  addCategory(categoryDet){
    categoryDet.uid = this.usrSession.merchantID;
    this.onLineProv.addCategory(categoryDet).then(resp=>{
      var respObj:any=resp;
      alert(respObj.message);
      this.updateCategoryStack(respObj.data);

    });
  }

   
  fetchProp(query){
    this.onLineProv.fetchStoreProp(query).then(resp=>{
        var respObj:any=resp;
        if(respObj.respcode=="00"){
          this.myStaffs = respObj.data.users;
          this.myProducts = respObj.data.products;
          this.myCategories = respObj.data.categories;
          var pageHeaderont = respObj.data.store.settings.pageHeader;
          var pageFooter = respObj.data.store.settings.pageFooter;
          if(pageHeaderont!=null){
            this.pageHeader = pageHeaderont;
          }
          if(pageFooter!=null&&pageFooter.length>0){
            this.pageFooter = pageFooter;
          }          
          console.log("I have again: "+JSON.stringify(this.pageFooter))
        }
    }).catch(err=>{
      console.log("issue fetcching properties is: "+err)
    });
  }

  saveProp(){
    this.newProduct.uid = this.usrSession.merchantID;
    this.propSetings = {pageHeader:this.pageHeader,pageFooter:this.pageFooter};
    var updateCont = {merchantID:this.usrSession.merchantID,merchantProp:this.propSetings};
    console.log("Sending :"+JSON.stringify(updateCont))
    this.onLineProv.updateMerchantProp(updateCont).then(resp=>{
      var respObj:any=resp;
      alert(respObj.message);
    });
  }
  addToColor(){
    this.colorStack.push(this.colorPicked);
  }

  addToSize(){
    this.sizeStack.push(this.sizePicked);
  }

  removeFromColor(color){

  }

  removeFromSize(size){

  }

  navToProductdetails(selectedProduct:Product){
    this.router.navigate(['product_detail',{'data':selectedProduct.productID}]);
  }

  switchView(ativeView){
    this.navStack.push(ativeView);
    var stackSze = this.navStack.length;
    this.activeView = this.navStack[stackSze-1];
  }

  navToBack(){
    if(this.navStack.length==1){
      return;
    }
    this.navStack.pop();
    var stackSze = this.navStack.length;
    this.activeView = this.navStack[stackSze-1];
    this.navStackOpt={mode:'list',values:''};

  }

  switchNavStack(mode,stackData){
    this.navStackOpt.mode = mode;
    var activeView = this.navStack[this.navStack.length-1];
    if(mode =="view"){
      this.determineContentData(activeView,stackData);
      mode = activeView+"."+mode
      this.navStack.push(mode);
    }else if(mode =="update"){
      this.updateContent(activeView,stackData);
      this.navStack.pop();
    }else if(mode =="new"){
      this.determineContentData(activeView,stackData);
      mode = activeView+"."+mode
      this.navStack.push(mode);
    }else if(mode =="add"){
      alert("would add later")
      this.navStack.pop();
    }
    
    
  }

  determineContentData(activeView,contentData){
    // alert(activeView)
    if(activeView =="stockMgmt.category"){
      this.newCategory = contentData;
    }else if(activeView =="stockMgmt.product"){
      this.newProduct = contentData;
    }else if(activeView=="usrMgmt"){
      this.staffDetail = contentData;
    }
  }

  updateContent(activeView,contentData){
    if(activeView == "stockMgmt.category.view"){
      console.log(JSON.stringify(contentData));
    }
  }

   
  changeImage(){
    alert("changing image now");
  }

  updateCategoryStack(categoryDet){
    var doPush =true;
    for(var i=0;i<this.myCategories.length;i++){
      console.log("gt "+this.myCategories[i].id+" and"+categoryDet.id);
      if(this.myCategories[i].id == categoryDet.id){
          doPush =false;
      } 
    }
    if(doPush){
      this.myCategories.push(categoryDet);
    }
  }

  updateProductStack(productDetail){
    var doPush =true;
    for(var i=0;i<this.myProducts.length;i++){
      if(this.myProducts[i].productID == productDetail.productID){
          doPush =false;
      } 
    }
    if(doPush){
      this.myProducts.push(productDetail);
    }
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }
  
  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();
  
    myReader.onloadend = (e) => {
      this.newProduct.image = myReader.result;
    }
    myReader.readAsDataURL(file);
  }
  
}
