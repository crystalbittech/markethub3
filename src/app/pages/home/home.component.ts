import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatasharingService } from '../../services/datasharing/datasharing.service';
import {User, UserSession} from '../../models/user';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
import {ReactiveFormsModule } from '@angular/forms'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  searchCriteria={'name':'','location':''};
  isUsrLoggedin:boolean=false;
  userSession:UserSession={};
  suggestions:any=[];
  hidesuggestion:boolean=true;
  constructor(
    private routes:Router,
    private dataSharer:DatasharingService,
    private backApi:OnlineproviderService
  ) { 


  }

  ngOnInit() {
    this.dataSharer.storeEditMode.next(false);
    this.userSession = this.dataSharer.getSession();
    // this.dataSharer.userSession.next(this.userSession);what is he doing
  }

  navToSearchResult(){
    if(this.searchCriteria.name==""){
      alert("Enter a Produt or service")
      return false;
    }
    console.log("About doing Search using "+this.searchCriteria.name+" and "+this.searchCriteria.location);
    this.routes.navigate(['searchresult/'+this.searchCriteria.name]);
  }

  searchSuggestion(searchInput){
    console.log("Typeing: "+searchInput)
    this.suggestions=[];
    this.backApi.searchItem(searchInput).then(resp=>{
      var respObj:any = resp;
      var resCont= respObj.message;
      for(var i=0;i<resCont.length;i++){
        var resObj = resCont[i].metadata
        for(var j=0;j<resObj.length;j++){
          if(resCont[i].searcType=='product'){
            this.suggestions.push({title:resObj[j].productName,type:'product',id:resObj[j].productID})
          }else{
            this.suggestions.push({title:resObj[j].bizName,type:'store',id:resObj[j].merchantID})
          }
          
        }
      }
      this.hidesuggestion = false;
      
      
  }).then(resp=>{
    if(this.suggestions.length==0){
      this.hidesuggestion = true;
    }else{
    }
  })
  }

  hideSuggest(selctedVal){
    this.hidesuggestion = true;
    this.searchCriteria.name = selctedVal;
  }
}
