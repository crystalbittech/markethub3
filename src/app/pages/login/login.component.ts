import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
import {DatasharingService} from '../../services/datasharing/datasharing.service';
import {User,UserSession} from '../../models/user';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
userInputs:User={
  email:'shoro@sho.com',
  password:'admin@123'
}
userSession:UserSession={};

  constructor(
    private router:Router,
    private backApi:OnlineproviderService,
    private dataSharer:DatasharingService,
    private cookieService:CookieService
  ) { }

  ngOnInit() {
    console.log("Entered Login");
    if(this.cookieService.get( 'isUserLoggedIn')=='true'){
      this.router.navigate(['login']);
    }
  }

  login(){
    this.backApi.signIn(this.userInputs).then(resp=>{
      var respObj:any=resp;
      if(respObj.message != "user exists"){
        alert(respObj.message);
      }else{
        console.log("val here is: "+JSON.stringify(respObj.data));
        this.cookieService.set( 'sessionID', respObj.data.uid);
        this.cookieService.set( 'isUserLoggedIn', 'true');//should be fetched from api
        this.backApi.fetchSession(respObj.data.uid).then(resp=>{
          var resObj:any = resp;
          this.userSession = resObj;
          this.dataSharer.userSession.next(this.userSession);
          this.router.navigate(['index']);
        })
        
      }
    });
  }
}
