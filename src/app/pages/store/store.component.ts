import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {OnlineproviderService} from './../../services/onlineprovider/onlineprovider.service';
import {Product} from './../../models/product';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
storeContents:[Product];
  constructor(
      private router:Router,
      private onlineProv:OnlineproviderService,
      private activeRoute:ActivatedRoute
  ){ 
      var storeID = this.activeRoute.snapshot.params['data'];
      if(storeID==null||storeID==''){
        alert("Store Page does not Exist or has been Removed");
        this.router.navigate(['searchresult/store']);
      }else{
        console.log("Fetching product detail with: "+storeID);
        this.onlineProv.fetchStoreDEtails(storeID).then(resp=>{
          var prodObj:any = resp;
          this.storeContents = prodObj.data.products;
        })
      }
   }

  ngOnInit() {
  }

  navToDetails(){
    alert('navigating');
    this.router.navigate['product_detail'];
  }

}
