import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Merchant} from '../../models/merchant';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
import { Product } from '../../models/product';
import { Category } from '../../models/category';
import {DatasharingService} from '../../services/datasharing/datasharing.service';


@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {
  merchantList:[Merchant];
  allCategories:[Category];
  categoryList:any=[];
  lat: number = 6.5503636;
  lng: number = 3.2998000999999997;
  lat2: number = 6.3503636;
  lng2: number = 3.6998000999999997;
  mapType:string="roadmap";
  zoomLevel:number=17;
  counteer:number=0;

 
  constructor(
    private routes:Router,
    private roueAct:ActivatedRoute,
    private dataSharer:DatasharingService,
    private backApi:OnlineproviderService
  ) { 
      this.backApi.fetchAllCategory().then(resp=>{
          var respObj:any = resp;
          this.allCategories = respObj;
          for(var i=0;i<this.allCategories.length;i++){
            let isAdded=false; 
            if(i==0){
              this.categoryList.push(this.allCategories[i].categoryName);
              continue;
            }
            for(var j=0;j<this.categoryList.length;j++){
              if(this.allCategories[i].categoryName.toUpperCase()==this.categoryList[j].toUpperCase()){
                isAdded = true;
              }
            }
            if(!isAdded){
              this.categoryList.push(this.allCategories[i].categoryName);
            }
          }
      });
    }
    
  ngOnInit(){
    this.backApi.fetchMerchants().then(resp=>{
      var respObj:any = resp;
      this.merchantList = respObj;
    })
  }
 
  navToStore(storeID){
    this.routes.navigate(['store',{'data':storeID}]);
  }
}
