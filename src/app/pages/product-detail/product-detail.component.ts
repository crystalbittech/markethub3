import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {Product} from '../../models/product';
import {OnlineproviderService} from './../../services/onlineprovider/onlineprovider.service';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
productDetail:Product={};
searchList:any=[]
  constructor(private router:Router,
    private routerAct:ActivatedRoute,
    private onlineProv:OnlineproviderService
  ) {
    
    var productID = this.routerAct.snapshot.params['data'];
    if(productID==null||productID==''){
      alert("Product Page does not Exist or has been Removed");
      this.router.navigate(['store']);
    }else{
      console.log("Fetching product detail with: "+productID);
      this.onlineProv.fetchSngProduct(productID).then(resp=>{
        var prodObj:any = resp;
        this.productDetail = prodObj[0];
      })
    }
  }

  ngOnInit() {
  }

  
}
