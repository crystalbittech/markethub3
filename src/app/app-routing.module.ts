import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {RecoverComponent} from './pages/recover/recover.component';
import {SignupComponent} from './pages/signup/signup.component';
import {HomeComponent} from './pages/home/home.component';
import {SearchresultComponent} from './pages/searchresult/searchresult.component';
import {ProductsComponent} from './pages/products/products.component';
import {StoreComponent} from  './pages/store/store.component';
import {StoresComponent} from  './pages/stores/stores.component';
import {ProductDetailComponent} from './pages/product-detail/product-detail.component';
import {ContactComponent} from './pages/contact/contact.component';
import {StoreEditComponent} from './pages/store-edit/store-edit.component';
import {CartComponent} from './pages/cart/cart.component';
import {SellComponent} from './pages/sell/sell.component';
import {AdvertiseComponent} from './pages/advertise/advertise.component';

import {RouterGaurdService} from './services/router-gaurd/router-gaurd.service';
const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'index', component: HomeComponent,},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: SignupComponent},
  { path: 'recover', component: RecoverComponent},
  { path: 'searchresult/:data', component: SearchresultComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'stores', component: StoresComponent},
  { path: 'store', component: StoreComponent},
  { path: 'store_edit', component: StoreEditComponent,canActivate:[RouterGaurdService]},
  { path: 'product_detail', component: ProductDetailComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'sell', component: SellComponent,canActivate:[RouterGaurdService]},
  { path: 'advert', component: AdvertiseComponent,canActivate:[RouterGaurdService]},
  { path: 'cart', component: CartComponent},
  { path: '**', component: HomeComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}